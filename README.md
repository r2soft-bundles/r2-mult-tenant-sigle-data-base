## Configuração do middleware

Na propriedade $routeMiddleware da classe Kernel do laravel, que fica em app/Http/Kernel.php
Adicionar **'tenant'=> \R2Soft\Tenant\Middleware\TenantMiddleware::class** dentro do array.


## Configuração de usuário com AuthPostgres

No .env da aplicação deve adicionar uma senha default para quando for criado o usuário no banco de dados sera setado essa senha;

**DEFAULT_PASSWORD_TENANT=senha_que_vai_ser_usada**


Quando for criar novo usuário no sistema, deve criar um novo usuário no banco de dados também, da seguinte forma.

**$authUser = new AuthPostgres();**
**if(!$authUser->usuarioExist($user->email)){**
    
    $authUser->addUser($user->email);
    
**}**


Quando for atualizar o usuário no sistema, deve atualizar o usuário do banco de dados também da seguinte forma.

**(new AuthPostgres())->updateUser($emailAnterior, $user->email);**
 
## Adicionar prefix para o usuário do banco de dados

No .env devera ser adicionado a variavel de ambiente
 
**PREFIX_USER_DB_TENANT=prefixo**











 