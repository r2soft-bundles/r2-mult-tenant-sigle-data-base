<?php

namespace R2Soft\Tenant;

use R2Soft\Tenant\AuthPostgres\AuthPostgres;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class TenantManager
{

    public function __construct(){}


    public function connect()
    {
        $user = Auth::user();
        $authPassword = (new AuthPostgres())->getSenha();

        try {
            DB::purge("tenant");
            Config::set('database.connections.tenant.host', getenv('DB_HOST'));
            Config::set('database.connections.tenant.database', getenv('DB_DATABASE'));
            Config::set('database.connections.tenant.username', getenv('PREFIX_USER_DB_TENANT').'-'.$user->email);
            Config::set('database.connections.tenant.password', $authPassword);
            Config::set('database.connections.tenant.schema', 'public');
            Config::set('database.connections.tenant.charset', 'utf8');
            Config::set('database.connections.tenant.port', getenv('DB_PORT'));
            Config::set('database.connections.tenant.driver', 'pgsql');
            Config::set('database.connections.tenant.application_name', env('DB_APPLICATION_NAME', $user->institution_id));
            Config::set('database.default', 'tenant');
            DB::connection("tenant");

            $this->checkConnection();
        } catch(\PDOException $e) {
            throw new \Exception('Acesso inválido');

        } catch (\ErrorException $e) {
            throw new \Exception("Acesso inválido!");
        }
    }

    private function checkConnection()
    {
        try{
            return DB::connection ( 'tenant' )->statement ( "SELECT 1 = 1;" );
        }catch(\Exception $e){
            throw new \Exception("Acesso inválido! A empresa não possui configuração de banco de dados válida. Entre em contato com o suporte!", $e->getCode());
        }
    }
}
