<?php

namespace R2Soft\Tenant\AuthPostgres;


use Illuminate\Support\Facades\DB;

class AuthPostgres
{

    private $senha;

    public function __construct()
    {
        $this->senha = getenv('DEFAULT_PASSWORD_TENANT');
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function usuarioExist(string $username)
    {
        $username = getenv('PREFIX_USER_DB_TENANT').'-'.$username;
        if (DB::select("SELECT usename FROM pg_user where usename = '{$username}';")) {
            return true;
        }
        return false;
    }

    public function addUser(string $username)
    {
        $userDb = getenv('PREFIX_USER_DB_TENANT').'-'.$username;
        return DB::select("CREATE ROLE \"{$userDb}\" PASSWORD '{$this->getSenha()}' SUPERUSER NOREPLICATION LOGIN;");
    }

    public function deleteUser(string $username)
    {
        $username = getenv('PREFIX_USER_DB_TENANT').'-'.$username;
        DB::select('DROP ROLE "'.$username.'";');
    }

    public function updateUser(string $userNameAntigo, string $userNameNovo)
    {
        $userNameAntigo = getenv('PREFIX_USER_DB_TENANT').'-'.$userNameAntigo;
        $userNameNovo = getenv('PREFIX_USER_DB_TENANT').'-'.$userNameNovo;
        if ($userNameAntigo<>$userNameNovo) {
            return DB::select("ALTER ROLE \"{$userNameAntigo}\" RENAME TO \"{$userNameNovo}\";");
        } else {
            return DB::select("ALTER ROLE \"{$userNameNovo}\" PASSWORD  '{$this->getSenha()}'");
        }
    }

}