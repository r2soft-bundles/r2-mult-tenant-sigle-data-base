<?php

namespace R2Soft\Tenant\Middleware;;

use \R2Soft\Tenant\TenantManager;
use Closure;
use Illuminate\Support\Facades\Auth;

class TenantMiddleware
{

    private $tenantManager;

    public function __construct(TenantManager $tenantManager)
    {
        $this->tenantManager = $tenantManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $this->tenantManager->connect();
        } catch (\Exception $e) {
            Auth::logout();
            return redirect()->route('login')->with ('message', $e->getMessage());
        }
        return $next($request);
    }
}
